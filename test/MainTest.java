import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static org.junit.Assert.assertEquals;

/**
 * User: hannes
 * Date: 12/14/13
 * Time: 9:46 PM
 */
public class MainTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    @Before
    public void setStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void cleanStreams() {
        System.setOut(null);
        System.setErr(null);
        System.setIn(null);
    }

    @Test
    public void testBoolean() throws Exception {
        String code = "m = tõene\n kui m!=väär siis prindi 1";
        runProgram(code);
        assertEquals("1\n", outContent.toString());

    }

    @Test
    public void testBubbleSort() throws Exception {
        String code = "i = 0;n = 5;\n" +
                "kuni i<n {\n" +
                "    arv[i] = n-i;\n" +
                "    i = i + 1;\n" +
                "} */\n" +
                "\n" +
                "//sorteeri\n" +
                "i = 0;\n" +
                "kuni i<n {\n" +
                "    j=0;\n" +
                "    kuni j<n-1 {\n" +
                "        kui arv[j] > arv[j+1] siis {\n" +
                "            ajutine = arv[j+1];\n" +
                "            arv[j+1] = arv[j];      //assign 4\n" +
                "            arv[j] = ajutine;\n" +
                "        }\n" +
                "        j=j+1;\n" +
                "    }\n" +
                "        i=i+1;\n" +
                "}\n" +
                "i = 0;\n" +
                "kuni i<n {\n" +
                "    prindi arv[i];\n" +
                "    i = i + 1;\n" +
                "}\n";
        runProgram(code);
        assertEquals("1\n2\n3\n4\n5\n", outContent.toString());

    }

    @Test
    public void testArray() throws Exception {
        String code = "i=0;\n" +
                "KUNI i<10 {\n" +
                "    massiiv[i] = i*2;\n" +
                "    i = i+1\n" +
                "}\n" +
                "\n" +
                "i=0;\n" +
                "KUNI i<10 {\n" +
                "    PRINDI massiiv[i];\n" +
                "    i=i+1;\n" +
                "}";
        runProgram(code);
        String expected = "0\n" +
                "2\n" +
                "4\n" +
                "6\n" +
                "8\n" +
                "10\n" +
                "12\n" +
                "14\n" +
                "16\n" +
                "18\n";
        assertEquals( expected, outContent.toString());

    }

    @Test
    public void testIf() throws Exception {
        // 12 > 10
        String code = "a = 10; " +
                      "if( a == 10 ) kirjuta \"true\";";
        runProgram(code);
        assertEquals("true\n", outContent.toString());
        outContent.reset();

        // 12 < 10
        code = "a = \"test\"; " +
                "if( a == \"test\" ) kirjuta \"true\";";
        runProgram(code);
        assertEquals("true\n", outContent.toString());
        outContent.reset();

        // messytest
        code = "a = 2;\n" +
                "b = 3;\n" +
                "c = 4;\n" +
                "d = 3;\n" +
                "kui (b!=d) siis {\n" +
                "    kirjuta \"1\";\n" +
                "} MUIDU KUI (b<=10) siis\n" +
                "kui d>a*2 SIIS kirjuta 2\n" +
                "muidu kirjuta 3;";
        runProgram(code);
        assertEquals("3\n", outContent.toString());
    }

    @Test
    public void testMultiplication() throws Exception {
        int a = 13;
        int b = 23;
        doArithm(a, b, "a*b");
        double mult = a*b;
        assertEquals(mult, Double.parseDouble(outContent.toString()), 1);
    }

    @Test
    public void testAddition() throws Exception {
        int a = 1343;
        int b = -233;
        doArithm(a, b, "a+b");
        double add = a+b;
        assertEquals(add, Double.parseDouble(outContent.toString()), 1);

        outContent.reset();
        String code = "a = 1;\n" +
                "prindi (a+2);";
        runProgram(code);
        assertEquals("3\n", outContent.toString());
    }
    @Test
    public void testSubtraction() throws Exception {
        int a = 1343;
        int b = -233;
        doArithm(a, b, "a-b");
        double min = a-b;
        assertEquals(min, Double.parseDouble(outContent.toString()), 1);
    }
    @Test
    public void testDivision() throws Exception {
        int a = 1343;
        int b = -233;
        doArithm(a, b, "a/b");
        double div = a/b;
        assertEquals(div, Double.parseDouble(outContent.toString()), 1);
    }

    private void doArithm(int a, int b, String eq) {
        String code = "a = " + a +"; b = " + b +"; d = "+eq+"; kirjuta d;";
        runProgram(code);
    }

    @Test
    public void testOutput() throws Exception {
        String inputString = "kirjuta \"hello\"";
        runProgram(inputString);
        assertEquals("hello\n", outContent.toString());
    }

    @Test
    public void testInput() throws Exception {
        String inputCode = "loe a\nkirjuta a \" was written.\"";
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream("test\n".getBytes());
        System.setIn(byteArrayInputStream);
        runProgram(inputCode);
        assertEquals("test was written.\n", outContent.toString());
    }

    private void runProgram(String inputString) {
        // feed input stream to antlr
        ANTLRInputStream input = new ANTLRInputStream(inputString);
        CompLexer lexer = new CompLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CompParser parser = new CompParser(tokens);
        ParseTree tree = parser.prog();

        //walk tree
        EvalVisitor eval = new EvalVisitor();
        eval.visit(tree);
    }
}
