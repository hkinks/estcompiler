// Generated from Comp.g4 by ANTLR 4.0
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.ParserRuleContext;

public class CompBaseVisitor<T> extends AbstractParseTreeVisitor<T> implements CompVisitor<T> {
	@Override public T visitAssign(CompParser.AssignContext ctx) { return visitChildren(ctx); }

	@Override public T visitInt(CompParser.IntContext ctx) { return visitChildren(ctx); }

	@Override public T visitBlock(CompParser.BlockContext ctx) { return visitChildren(ctx); }

	@Override public T visitParens(CompParser.ParensContext ctx) { return visitChildren(ctx); }

	@Override public T visitWhenStmt(CompParser.WhenStmtContext ctx) { return visitChildren(ctx); }

	@Override public T visitDouble(CompParser.DoubleContext ctx) { return visitChildren(ctx); }

	@Override public T visitScanArr(CompParser.ScanArrContext ctx) { return visitChildren(ctx); }

	@Override public T visitArrMultiAssign(CompParser.ArrMultiAssignContext ctx) { return visitChildren(ctx); }

	@Override public T visitConcat(CompParser.ConcatContext ctx) { return visitChildren(ctx); }

	@Override public T visitAndOp(CompParser.AndOpContext ctx) { return visitChildren(ctx); }

	@Override public T visitComparison(CompParser.ComparisonContext ctx) { return visitChildren(ctx); }

	@Override public T visitId(CompParser.IdContext ctx) { return visitChildren(ctx); }

	@Override public T visitAddsub(CompParser.AddsubContext ctx) { return visitChildren(ctx); }

	@Override public T visitProg(CompParser.ProgContext ctx) { return visitChildren(ctx); }

	@Override public T visitOrOp(CompParser.OrOpContext ctx) { return visitChildren(ctx); }

	@Override public T visitPrintExpr(CompParser.PrintExprContext ctx) { return visitChildren(ctx); }

	@Override public T visitString(CompParser.StringContext ctx) { return visitChildren(ctx); }

	@Override public T visitMuldiv(CompParser.MuldivContext ctx) { return visitChildren(ctx); }

	@Override public T visitScan(CompParser.ScanContext ctx) { return visitChildren(ctx); }

	@Override public T visitBool(CompParser.BoolContext ctx) { return visitChildren(ctx); }

	@Override public T visitIfStmt(CompParser.IfStmtContext ctx) { return visitChildren(ctx); }

	@Override public T visitNull(CompParser.NullContext ctx) { return visitChildren(ctx); }

	@Override public T visitArrSingleAssign(CompParser.ArrSingleAssignContext ctx) { return visitChildren(ctx); }

	@Override public T visitArray(CompParser.ArrayContext ctx) { return visitChildren(ctx); }
}