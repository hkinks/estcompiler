grammar Comp;

//parser rules
prog:  (stmt SEMICOLON?)+;    // semicolon is optional

stmt:   PRINT expr                                                            # printExpr
    |   SCAN ID                                                               # scan
    |   SCAN ID '[' index=expr ']'                                            # scanArr
    |   ID '=' expr                                                           # assign
    |   ID '[' index=expr ']' '=' val=expr                                    # arrSingleAssign
    |   ID '[' (size=expr)? ']' '=' '{'expr (',' expr)* '}'                   # arrMultiAssign
    |   IF '('? cond=expr ')'? THEN thenBlock=block (ELSE elseBlock=block)?   # ifStmt
    |   WHILE '('? cond=expr ')'? repeatBlock=block                           # whenStmt
    ;

expr:   expr op=('*'|'/') expr                                                # muldiv
    |   expr op=('+'|'-') expr                                                # addsub
    |   expr op=('<'|NEQ|'>'|'<='|'>='|'==') expr                             # comparison
    |   expr op=AND expr                                                      # andOp        //todo
    |   expr op=OR expr                                                       # orOp        //todo
    |   expr expr                                                             # concat
    |   ID '[' index=expr ']'                                                 # array
    |   ID                                                                    # id
    |   '(' expr ')'                                                          # parens
    |   NULL                                                                  # null
    |   (TRUE | FALSE)                                                        # bool
    |   '-'? INT                                                              # int
    |   '-'? FLOAT                                                            # double
    |   STRING                                                                # string
;

block:  stmt SEMICOLON?
    |   '{' (stmt SEMICOLON?)* '}'
    ;


//lexical(token) rules
INT: [0-9]+;
FLOAT: [0-9]*'.'[0-9]+;
STRING: '"' (~[\\"]|'\\\\'|'\\"')*? '"';
NEQ: '!=' | '<>';
PLUS: '+';
MINUS: '-';
MUL: '*';
DIV: '/';
LT: '<';
LTE: '<=';
EQ: '==';
GTE: '>=';
GT: '>';
COLON: ':';
SEMICOLON: ';';
ASSIGN: '=';
DOT: '.';
RPAREN: '(';
LPAREN: ')';
RBR: '{';
LBR: '}';
RB: '[';
LB: ']';
COMMA: ',';
SCAN: 'loe' | 'LOE';
PRINT: 'kirjuta' | 'KIRJUTA' | 'prindi' | 'PRINDI';
IF: 'KUI' | 'kui';
THEN: 'siis' | 'SIIS';
ELSE: 'muidu' | 'vastasel juhul' | 'MUIDU' | 'VASTASEL JUHUL';
WHILE: 'kuni' | 'KUNI';
AND: 'ja' | 'JA' | '&&';
OR: 'või' | 'VÕI' |'||';
NULL: 'null' | 'NULL';
NOT: 'mitte' | '!' | 'MITTE';
TRUE: 'tõene' | 'TÕENE' ;
FALSE: 'VÄÄR' | 'väär' ;
ID: [_a-zA-Z] [_a-zA-Z0-9]*;
COMMENT: '//' .*? ('\n'|EOF) -> skip;
MULTILINECOMMENT: '/*' .*? '*/' -> skip;
EOL: [ \t\r\n] -> skip;

