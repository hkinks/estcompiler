import org.antlr.v4.runtime.ParserRuleContext;

import java.util.*;

/**
 * User: hannes
 * Date: 12/13/13
 * Time: 11:06 PM
 */
public class EvalVisitor extends CompBaseVisitor<Object> {
    // variables are stored as strings(which is bad) to eliminate variable types
    // variable memory
    Map<String, String> mem = new HashMap<String, String>();
    // array memory
    Map<String, ArrayList<String>> aMem = new HashMap<String, ArrayList<String>>();

    /** assign */
    @Override
    public String visitAssign(CompParser.AssignContext ctx) {
        String id = ctx.ID().getText(); // get variable name
        String value = String.valueOf(visit(ctx.expr()));  // visit expression on the right for value
        if(mem.containsKey(id)) mem.remove(id);
        mem.put(id, value);             // store in memory
        return value;
    }

    /** multi assignment for arrays ( ie array[] = {1, 2, 4} */
    @Override
    public Object visitArrMultiAssign(CompParser.ArrMultiAssignContext ctx) {
        ArrayList<String> array = new ArrayList<String>();
        String id = ctx.ID().getText();
        aMem.put(id, array);
        List<CompParser.ExprContext> list = ctx.expr();
        for (CompParser.ExprContext expr : list) {  // iterate over variables in brackets and add them to array
            array.add(String.valueOf(visit(expr)));
        }
        return null;
    }

    /** assign array */
    @Override
    public Object visitArrSingleAssign(CompParser.ArrSingleAssignContext ctx) {
        ArrayList<String> array;
        String id = ctx.ID().getText();
        if (aMem.containsKey(id)) array = aMem.get(id);   // check if array already exists
        else {
            array = new ArrayList<String>(); // else create new one
            aMem.put(id, array);             // and save in memory
        }
        try {
            int index = Integer.parseInt(visit(ctx.index).toString()); // try to get integer
            // if trying to assign to index that is +1 larger than array, simply add it to the end
            if (index == array.size()) array.add(index, visit(ctx.val).toString());
            // else if trying to assign to index way larger than array, then throw exception
            else if (index > array.size()) throw new LanguageException("Masiivi indeks on piiridest väljas.", ctx.getStart().getLine());
            else array.set(index, visit(ctx.val).toString());
        } catch ( NumberFormatException e ) {
            throw new LanguageException("Massiivi indeks peab olema täisarv.", ctx.getStart().getLine());
        }
        return null;
    }

    /** Print expression */
    @Override
    public String visitPrintExpr(CompParser.PrintExprContext ctx) {

        String value = visit(ctx.expr()).toString();
        System.out.println(value);
        return null;
    }

    /** read value in */
    @Override
    public String visitScan(CompParser.ScanContext ctx) {
        Scanner scanner = new Scanner(System.in);
        String buf = scanner.nextLine();
        String id = ctx.ID().getText();
        mem.put(id, buf);
        return buf;
    }

    /** scan array */
    @Override
    public Object visitScanArr(CompParser.ScanArrContext ctx) {
        Scanner scanner = new Scanner(System.in);
        String buf = scanner.nextLine();
        ArrayList<String> array;
        String id = ctx.ID().getText();
        if (aMem.containsKey(id)) array = aMem.get(id);
        else {
            array = new ArrayList<String>();
            aMem.put(id, array);
        }
        try {
            int index = Integer.parseInt((String) visit(ctx.index));
            array.add(index, buf);
        } catch ( Exception e ) {
            throw new LanguageException("Massiivi indeks peab olema täisarv.", ctx.getStart().getLine());
        }
        return null;
    }

    /** string */
    @Override
    public String visitString(CompParser.StringContext ctx) {
        String stringWithQuotes = ctx.STRING().getText();
        int len = stringWithQuotes.length();
        return stringWithQuotes.substring(1, len - 1); //remove quotes from string
    }

    /** double */
    @Override
    public Double visitDouble(CompParser.DoubleContext ctx) {
         return Double.valueOf(ctx.getText());
    }

    /** int */
    @Override
    public Integer visitInt(CompParser.IntContext ctx) {
        return Integer.valueOf(ctx.getText());
    }

    /** boolean */
    @Override
    public Object visitBool(CompParser.BoolContext ctx) {
        return ctx.TRUE() != null;
    }

    /** id */
    @Override
    public String visitId(CompParser.IdContext ctx) {
        String id = ctx.ID().getText();
        if( mem.containsKey(id) ) return mem.get(id);
        return null;
    }

    /** array */
    @Override
    public Object visitArray(CompParser.ArrayContext ctx) {
        String id = ctx.ID().getText();
        try{
            if( aMem.containsKey(id) && ctx.index != null )
                return aMem.get(id).get(Integer.parseInt(visit(ctx.index).toString()));
        } catch (IndexOutOfBoundsException e) {
            throw new LanguageException("Massiivi indeks on piiridest väljas. Indeks: "+visit(ctx.index) +
                    " Masiivi suurus:"+ aMem.get(id).size(), ctx.getStart().getLine());
        }
        return null;
    }

    /** Multiplication and division */
    @Override
    public String visitMuldiv(CompParser.MuldivContext ctx) {
        String left = String.valueOf(visit(ctx.expr(0)));
        String right = String.valueOf(visit(ctx.expr(1)));
        if (!(isNumber(left) && isNumber(right))) {
            throw new LanguageException("Sisestatud väärtustega pole võimalik tehet teostada.", ctx.getStart().getLine());
        }
        return operation(left, right, ctx.op.getType());
    }

    /** addition and subtraction */
    @Override
    public String visitAddsub(CompParser.AddsubContext ctx) {
        String left = String.valueOf(visit(ctx.expr(0)));
        String right = String.valueOf(visit(ctx.expr(1)));
        if (!(isNumber(left) && isNumber(right))) {
            throw new LanguageException("Sisestatud väärtustega pole võimalik tehet teostada.", ctx.getStart().getLine());
        }
        return operation(left, right, ctx.op.getType());
    }


    /** concatenation */
    @Override
    public String visitConcat(CompParser.ConcatContext ctx) {
        String left = visit(ctx.expr(0)).toString();
        String right = visit(ctx.expr(1)).toString();
        return left.concat(right);
    }

    private String operation(String left, String right, int type) {
        double result;
        switch (type) {
            case CompLexer.PLUS:
                result = Double.parseDouble(left) + Double.parseDouble(right);
                break;
            case CompLexer.MINUS:
                result = Double.parseDouble(left) - Double.parseDouble(right);
                break;
            case CompLexer.MUL:
                result = Double.parseDouble(left) * Double.parseDouble(right);
                break;
            case CompLexer.DIV:
                result = Double.parseDouble(left) / Double.parseDouble(right);
                break;
            default: result = 0;
        }
        //check if result is integer to remove trailing .0
        if (Math.ceil(result) == result) return String.valueOf((int)result);
        //or else return the value as double
        return String.valueOf(result);
    }

    /** parenthesis */
    @Override
    public String visitParens(CompParser.ParensContext ctx) {
        return (String) visit(ctx.expr()); // return child expr's value
    }

    /** comparison */
    @Override
    public Object visitComparison(CompParser.ComparisonContext ctx) {
        String a = String.valueOf(visit(ctx.expr(0)));
        String b = String.valueOf(visit(ctx.expr(1)));

        //check if operands to compare are numerical values
        double lnum = 0;
        double rnum = 0;
        boolean num = false;
        if(isNumber(a) && isNumber(b)) {
            num = true;
            lnum = Double.parseDouble(a);
            rnum = Double.parseDouble(b);
        }

        //check operator
        switch (ctx.op.getType()) {
            case CompLexer.EQ:
                return num ? (lnum==rnum) : (a.equals(b));
            case CompLexer.NEQ:
                return num ? (lnum!=rnum) : (!a.equals(b));
            case CompLexer.GT:
                if(num) return lnum > rnum; else throw new LanguageException("Operandid pole võrreldavad!", ctx.op.getLine());
            case CompLexer.GTE:
                if(num) return lnum >= rnum; else throw new LanguageException("Operandid pole võrreldavad!", ctx.op.getLine());
            case CompLexer.LT:
                if(num) return lnum < rnum; else throw new LanguageException("Operandid pole võrreldavad!", ctx.op.getLine());
            case CompLexer.LTE:
                if(num) return lnum <= rnum; else throw new LanguageException("Operandid pole võrreldavad!", ctx.op.getLine());
            default: throw new LanguageException("Määratud operaatorit ei leitud.", ctx.op.getLine());
        }
    }

    /** if statement */
    @Override
    public Object visitIfStmt(CompParser.IfStmtContext ctx) {
        try {
            if ((Boolean)visit(ctx.cond)){
                return visit(ctx.thenBlock);
            } else {
                if(ctx.elseBlock != null) return visit(ctx.elseBlock);
            }
        } catch (NullPointerException e){
            throw new LanguageException("Viga süntaksis.", ctx.getStart().getLine());
        }
        return null;
    }

    /** when */
    @Override
    public Object visitWhenStmt(CompParser.WhenStmtContext ctx) {
        while( (Boolean) visit(ctx.cond)) {
            visit(ctx.repeatBlock);
        }
        return null;
    }

    /** idiotic way to test if string is a number, todo using instanceof */
    private boolean isNumber(String test) {
        try {
            Double.parseDouble(test);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
