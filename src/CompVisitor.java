// Generated from Comp.g4 by ANTLR 4.0
import org.antlr.v4.runtime.tree.*;
import org.antlr.v4.runtime.Token;

public interface CompVisitor<T> extends ParseTreeVisitor<T> {
	T visitAssign(CompParser.AssignContext ctx);

	T visitInt(CompParser.IntContext ctx);

	T visitBlock(CompParser.BlockContext ctx);

	T visitParens(CompParser.ParensContext ctx);

	T visitWhenStmt(CompParser.WhenStmtContext ctx);

	T visitDouble(CompParser.DoubleContext ctx);

	T visitScanArr(CompParser.ScanArrContext ctx);

	T visitArrMultiAssign(CompParser.ArrMultiAssignContext ctx);

	T visitConcat(CompParser.ConcatContext ctx);

	T visitAndOp(CompParser.AndOpContext ctx);

	T visitComparison(CompParser.ComparisonContext ctx);

	T visitId(CompParser.IdContext ctx);

	T visitAddsub(CompParser.AddsubContext ctx);

	T visitProg(CompParser.ProgContext ctx);

	T visitOrOp(CompParser.OrOpContext ctx);

	T visitPrintExpr(CompParser.PrintExprContext ctx);

	T visitString(CompParser.StringContext ctx);

	T visitMuldiv(CompParser.MuldivContext ctx);

	T visitScan(CompParser.ScanContext ctx);

	T visitBool(CompParser.BoolContext ctx);

	T visitIfStmt(CompParser.IfStmtContext ctx);

	T visitNull(CompParser.NullContext ctx);

	T visitArrSingleAssign(CompParser.ArrSingleAssignContext ctx);

	T visitArray(CompParser.ArrayContext ctx);
}