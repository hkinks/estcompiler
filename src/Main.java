import java.io.InputStream;

import org.antlr.v4.gui.Trees;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import java.io.FileInputStream;

/**
 * User: hannes
 * Date: 12/13/13
 * Time: 10:22 PM
 */
public class Main {
    public static void main(String[] args) throws Exception{
        // if filename is included, read from file
        String inputFile = null;
        if ( args.length > 0 ) inputFile = args[0];
        // if no file submitted, read from standard input
        InputStream is = System.in;
        if ( inputFile != null ) is = new FileInputStream(inputFile);

        // feed input stream to antlr
        ANTLRInputStream input = new ANTLRInputStream(is);
        CompLexer lexer = new CompLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        CompParser parser = new CompParser(tokens);

        if (args.length == 2 && args[1].equals("--tree")) {
            //draw tree in gui
            ParserRuleContext ruleContext = parser.prog();

            /* updated in 4.5.1 */
            Trees.inspect(ruleContext, parser);
            /* deprecated */
//            ruleContext.inspect(parser);
        } else {
            ParseTree tree = parser.prog();
            //walk tree
            EvalVisitor eval = new EvalVisitor();
            eval.visit(tree);
        }
    }
}
