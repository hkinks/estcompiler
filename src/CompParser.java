// Generated from Comp.g4 by ANTLR 4.0
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CompParser extends Parser {
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		INT=1, FLOAT=2, STRING=3, NEQ=4, PLUS=5, MINUS=6, MUL=7, DIV=8, LT=9, 
		LTE=10, EQ=11, GTE=12, GT=13, COLON=14, SEMICOLON=15, ASSIGN=16, DOT=17, 
		RPAREN=18, LPAREN=19, RBR=20, LBR=21, RB=22, LB=23, COMMA=24, SCAN=25, 
		PRINT=26, IF=27, THEN=28, ELSE=29, WHILE=30, AND=31, OR=32, NULL=33, NOT=34, 
		TRUE=35, FALSE=36, FUNC=37, ID=38, COMMENT=39, MULTILINECOMMENT=40, EOL=41;
	public static final String[] tokenNames = {
		"<INVALID>", "INT", "FLOAT", "STRING", "NEQ", "'+'", "'-'", "'*'", "'/'", 
		"'<'", "'<='", "'=='", "'>='", "'>'", "':'", "';'", "'='", "'.'", "'('", 
		"')'", "'{'", "'}'", "'['", "']'", "','", "SCAN", "PRINT", "IF", "THEN", 
		"ELSE", "WHILE", "AND", "OR", "NULL", "NOT", "TRUE", "FALSE", "FUNC", 
		"ID", "COMMENT", "MULTILINECOMMENT", "EOL"
	};
	public static final int
		RULE_prog = 0, RULE_stmt = 1, RULE_expr = 2, RULE_block = 3;
	public static final String[] ruleNames = {
		"prog", "stmt", "expr", "block"
	};

	@Override
	public String getGrammarFileName() { return "Comp.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public CompParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class ProgContext extends ParserRuleContext {
		public TerminalNode SEMICOLON(int i) {
			return getToken(CompParser.SEMICOLON, i);
		}
		public List<TerminalNode> SEMICOLON() { return getTokens(CompParser.SEMICOLON); }
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public ProgContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_prog; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitProg(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ProgContext prog() throws RecognitionException {
		ProgContext _localctx = new ProgContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_prog);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(12); 
			_errHandler.sync(this);
			_la = _input.LA(1);
			do {
				{
				{
				setState(8); stmt();
				setState(10);
				_la = _input.LA(1);
				if (_la==SEMICOLON) {
					{
					setState(9); match(SEMICOLON);
					}
				}

				}
				}
				setState(14); 
				_errHandler.sync(this);
				_la = _input.LA(1);
			} while ( (((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SCAN) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << ID))) != 0) );
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class StmtContext extends ParserRuleContext {
		public StmtContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_stmt; }
	 
		public StmtContext() { }
		public void copyFrom(StmtContext ctx) {
			super.copyFrom(ctx);
		}
	}
	public static class AssignContext extends StmtContext {
		public TerminalNode ID() { return getToken(CompParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public AssignContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitAssign(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class PrintExprContext extends StmtContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode PRINT() { return getToken(CompParser.PRINT, 0); }
		public PrintExprContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitPrintExpr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ScanContext extends StmtContext {
		public TerminalNode ID() { return getToken(CompParser.ID, 0); }
		public TerminalNode SCAN() { return getToken(CompParser.SCAN, 0); }
		public ScanContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitScan(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class WhenStmtContext extends StmtContext {
		public ExprContext cond;
		public BlockContext repeatBlock;
		public TerminalNode WHILE() { return getToken(CompParser.WHILE, 0); }
		public BlockContext block() {
			return getRuleContext(BlockContext.class,0);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public WhenStmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitWhenStmt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ScanArrContext extends StmtContext {
		public ExprContext index;
		public TerminalNode ID() { return getToken(CompParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode SCAN() { return getToken(CompParser.SCAN, 0); }
		public ScanArrContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitScanArr(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrMultiAssignContext extends StmtContext {
		public ExprContext size;
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ID() { return getToken(CompParser.ID, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ArrMultiAssignContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitArrMultiAssign(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IfStmtContext extends StmtContext {
		public ExprContext cond;
		public BlockContext thenBlock;
		public BlockContext elseBlock;
		public BlockContext block(int i) {
			return getRuleContext(BlockContext.class,i);
		}
		public TerminalNode THEN() { return getToken(CompParser.THEN, 0); }
		public List<BlockContext> block() {
			return getRuleContexts(BlockContext.class);
		}
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public TerminalNode ELSE() { return getToken(CompParser.ELSE, 0); }
		public TerminalNode IF() { return getToken(CompParser.IF, 0); }
		public IfStmtContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitIfStmt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrSingleAssignContext extends StmtContext {
		public ExprContext index;
		public ExprContext val;
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public TerminalNode ID() { return getToken(CompParser.ID, 0); }
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ArrSingleAssignContext(StmtContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitArrSingleAssign(this);
			else return visitor.visitChildren(this);
		}
	}

	public final StmtContext stmt() throws RecognitionException {
		StmtContext _localctx = new StmtContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_stmt);
		int _la;
		try {
			setState(78);
			switch ( getInterpreter().adaptivePredict(_input,9,_ctx) ) {
			case 1:
				_localctx = new PrintExprContext(_localctx);
				enterOuterAlt(_localctx, 1);
				{
				setState(16); match(PRINT);
				setState(17); expr(0);
				}
				break;

			case 2:
				_localctx = new ScanContext(_localctx);
				enterOuterAlt(_localctx, 2);
				{
				setState(18); match(SCAN);
				setState(19); match(ID);
				}
				break;

			case 3:
				_localctx = new ScanArrContext(_localctx);
				enterOuterAlt(_localctx, 3);
				{
				setState(20); match(SCAN);
				setState(21); match(ID);
				setState(22); match(RB);
				setState(23); ((ScanArrContext)_localctx).index = expr(0);
				setState(24); match(LB);
				}
				break;

			case 4:
				_localctx = new AssignContext(_localctx);
				enterOuterAlt(_localctx, 4);
				{
				setState(26); match(ID);
				setState(27); match(ASSIGN);
				setState(28); expr(0);
				}
				break;

			case 5:
				_localctx = new ArrSingleAssignContext(_localctx);
				enterOuterAlt(_localctx, 5);
				{
				setState(29); match(ID);
				setState(30); match(RB);
				setState(31); ((ArrSingleAssignContext)_localctx).index = expr(0);
				setState(32); match(LB);
				setState(33); match(ASSIGN);
				setState(34); ((ArrSingleAssignContext)_localctx).val = expr(0);
				}
				break;

			case 6:
				_localctx = new ArrMultiAssignContext(_localctx);
				enterOuterAlt(_localctx, 6);
				{
				setState(36); match(ID);
				setState(37); match(RB);
				setState(39);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << INT) | (1L << FLOAT) | (1L << STRING) | (1L << MINUS) | (1L << RPAREN) | (1L << NULL) | (1L << TRUE) | (1L << FALSE) | (1L << ID))) != 0)) {
					{
					setState(38); ((ArrMultiAssignContext)_localctx).size = expr(0);
					}
				}

				setState(41); match(LB);
				setState(42); match(ASSIGN);
				setState(43); match(RBR);
				setState(44); expr(0);
				setState(49);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la==COMMA) {
					{
					{
					setState(45); match(COMMA);
					setState(46); expr(0);
					}
					}
					setState(51);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(52); match(LBR);
				}
				break;

			case 7:
				_localctx = new IfStmtContext(_localctx);
				enterOuterAlt(_localctx, 7);
				{
				setState(54); match(IF);
				setState(56);
				switch ( getInterpreter().adaptivePredict(_input,4,_ctx) ) {
				case 1:
					{
					setState(55); match(RPAREN);
					}
					break;
				}
				setState(58); ((IfStmtContext)_localctx).cond = expr(0);
				setState(60);
				_la = _input.LA(1);
				if (_la==LPAREN) {
					{
					setState(59); match(LPAREN);
					}
				}

				setState(62); match(THEN);
				setState(63); ((IfStmtContext)_localctx).thenBlock = block();
				setState(66);
				switch ( getInterpreter().adaptivePredict(_input,6,_ctx) ) {
				case 1:
					{
					setState(64); match(ELSE);
					setState(65); ((IfStmtContext)_localctx).elseBlock = block();
					}
					break;
				}
				}
				break;

			case 8:
				_localctx = new WhenStmtContext(_localctx);
				enterOuterAlt(_localctx, 8);
				{
				setState(68); match(WHILE);
				setState(70);
				switch ( getInterpreter().adaptivePredict(_input,7,_ctx) ) {
				case 1:
					{
					setState(69); match(RPAREN);
					}
					break;
				}
				setState(72); ((WhenStmtContext)_localctx).cond = expr(0);
				setState(74);
				_la = _input.LA(1);
				if (_la==LPAREN) {
					{
					setState(73); match(LPAREN);
					}
				}

				setState(76); ((WhenStmtContext)_localctx).repeatBlock = block();
				}
				break;
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ExprContext extends ParserRuleContext {
		public int _p;
		public ExprContext(ParserRuleContext parent, int invokingState) { super(parent, invokingState); }
		public ExprContext(ParserRuleContext parent, int invokingState, int _p) {
			super(parent, invokingState);
			this._p = _p;
		}
		@Override public int getRuleIndex() { return RULE_expr; }
	 
		public ExprContext() { }
		public void copyFrom(ExprContext ctx) {
			super.copyFrom(ctx);
			this._p = ctx._p;
		}
	}
	public static class IntContext extends ExprContext {
		public TerminalNode INT() { return getToken(CompParser.INT, 0); }
		public IntContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitInt(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ParensContext extends ExprContext {
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ParensContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitParens(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class DoubleContext extends ExprContext {
		public TerminalNode FLOAT() { return getToken(CompParser.FLOAT, 0); }
		public DoubleContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitDouble(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ConcatContext extends ExprContext {
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ConcatContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitConcat(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AndOpContext extends ExprContext {
		public Token op;
		public TerminalNode AND() { return getToken(CompParser.AND, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public AndOpContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitAndOp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ComparisonContext extends ExprContext {
		public Token op;
		public TerminalNode NEQ() { return getToken(CompParser.NEQ, 0); }
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public ComparisonContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitComparison(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class IdContext extends ExprContext {
		public TerminalNode ID() { return getToken(CompParser.ID, 0); }
		public IdContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitId(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class AddsubContext extends ExprContext {
		public Token op;
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public AddsubContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitAddsub(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class OrOpContext extends ExprContext {
		public Token op;
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public TerminalNode OR() { return getToken(CompParser.OR, 0); }
		public OrOpContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitOrOp(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class StringContext extends ExprContext {
		public TerminalNode STRING() { return getToken(CompParser.STRING, 0); }
		public StringContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitString(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class MuldivContext extends ExprContext {
		public Token op;
		public ExprContext expr(int i) {
			return getRuleContext(ExprContext.class,i);
		}
		public List<ExprContext> expr() {
			return getRuleContexts(ExprContext.class);
		}
		public MuldivContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitMuldiv(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class BoolContext extends ExprContext {
		public TerminalNode FALSE() { return getToken(CompParser.FALSE, 0); }
		public TerminalNode TRUE() { return getToken(CompParser.TRUE, 0); }
		public BoolContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitBool(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class NullContext extends ExprContext {
		public TerminalNode NULL() { return getToken(CompParser.NULL, 0); }
		public NullContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitNull(this);
			else return visitor.visitChildren(this);
		}
	}
	public static class ArrayContext extends ExprContext {
		public ExprContext index;
		public TerminalNode ID() { return getToken(CompParser.ID, 0); }
		public ExprContext expr() {
			return getRuleContext(ExprContext.class,0);
		}
		public ArrayContext(ExprContext ctx) { copyFrom(ctx); }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitArray(this);
			else return visitor.visitChildren(this);
		}
	}

	public final ExprContext expr(int _p) throws RecognitionException {
		ParserRuleContext _parentctx = _ctx;
		int _parentState = getState();
		ExprContext _localctx = new ExprContext(_ctx, _parentState, _p);
		ExprContext _prevctx = _localctx;
		int _startState = 4;
		enterRecursionRule(_localctx, RULE_expr);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(102);
			switch ( getInterpreter().adaptivePredict(_input,12,_ctx) ) {
			case 1:
				{
				_localctx = new ArrayContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;

				setState(81); match(ID);
				setState(82); match(RB);
				setState(83); ((ArrayContext)_localctx).index = expr(0);
				setState(84); match(LB);
				}
				break;

			case 2:
				{
				_localctx = new IdContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(86); match(ID);
				}
				break;

			case 3:
				{
				_localctx = new ParensContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(87); match(RPAREN);
				setState(88); expr(0);
				setState(89); match(LPAREN);
				}
				break;

			case 4:
				{
				_localctx = new NullContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(91); match(NULL);
				}
				break;

			case 5:
				{
				_localctx = new BoolContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(92);
				_la = _input.LA(1);
				if ( !(_la==TRUE || _la==FALSE) ) {
				_errHandler.recoverInline(this);
				}
				consume();
				}
				break;

			case 6:
				{
				_localctx = new IntContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(94);
				_la = _input.LA(1);
				if (_la==MINUS) {
					{
					setState(93); match(MINUS);
					}
				}

				setState(96); match(INT);
				}
				break;

			case 7:
				{
				_localctx = new DoubleContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(98);
				_la = _input.LA(1);
				if (_la==MINUS) {
					{
					setState(97); match(MINUS);
					}
				}

				setState(100); match(FLOAT);
				}
				break;

			case 8:
				{
				_localctx = new StringContext(_localctx);
				_ctx = _localctx;
				_prevctx = _localctx;
				setState(101); match(STRING);
				}
				break;
			}
			_ctx.stop = _input.LT(-1);
			setState(123);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!=-1 ) {
				if ( _alt==1 ) {
					if ( _parseListeners!=null ) triggerExitRuleEvent();
					_prevctx = _localctx;
					{
					setState(121);
					switch ( getInterpreter().adaptivePredict(_input,13,_ctx) ) {
					case 1:
						{
						_localctx = new MuldivContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(104);
						if (!(14 >= _localctx._p)) throw new FailedPredicateException(this, "14 >= $_p");
						setState(105);
						((MuldivContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==MUL || _la==DIV) ) {
							((MuldivContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						consume();
						setState(106); expr(15);
						}
						break;

					case 2:
						{
						_localctx = new AddsubContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(107);
						if (!(13 >= _localctx._p)) throw new FailedPredicateException(this, "13 >= $_p");
						setState(108);
						((AddsubContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !(_la==PLUS || _la==MINUS) ) {
							((AddsubContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						consume();
						setState(109); expr(14);
						}
						break;

					case 3:
						{
						_localctx = new ComparisonContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(110);
						if (!(12 >= _localctx._p)) throw new FailedPredicateException(this, "12 >= $_p");
						setState(111);
						((ComparisonContext)_localctx).op = _input.LT(1);
						_la = _input.LA(1);
						if ( !((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << NEQ) | (1L << LT) | (1L << LTE) | (1L << EQ) | (1L << GTE) | (1L << GT))) != 0)) ) {
							((ComparisonContext)_localctx).op = (Token)_errHandler.recoverInline(this);
						}
						consume();
						setState(112); expr(13);
						}
						break;

					case 4:
						{
						_localctx = new AndOpContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(113);
						if (!(11 >= _localctx._p)) throw new FailedPredicateException(this, "11 >= $_p");
						setState(114); ((AndOpContext)_localctx).op = match(AND);
						setState(115); expr(12);
						}
						break;

					case 5:
						{
						_localctx = new OrOpContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(116);
						if (!(10 >= _localctx._p)) throw new FailedPredicateException(this, "10 >= $_p");
						setState(117); ((OrOpContext)_localctx).op = match(OR);
						setState(118); expr(11);
						}
						break;

					case 6:
						{
						_localctx = new ConcatContext(new ExprContext(_parentctx, _parentState, _p));
						pushNewRecursionContext(_localctx, _startState, RULE_expr);
						setState(119);
						if (!(9 >= _localctx._p)) throw new FailedPredicateException(this, "9 >= $_p");
						setState(120); expr(0);
						}
						break;
					}
					} 
				}
				setState(125);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			unrollRecursionContexts(_parentctx);
		}
		return _localctx;
	}

	public static class BlockContext extends ParserRuleContext {
		public TerminalNode SEMICOLON(int i) {
			return getToken(CompParser.SEMICOLON, i);
		}
		public List<TerminalNode> SEMICOLON() { return getTokens(CompParser.SEMICOLON); }
		public StmtContext stmt(int i) {
			return getRuleContext(StmtContext.class,i);
		}
		public List<StmtContext> stmt() {
			return getRuleContexts(StmtContext.class);
		}
		public BlockContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_block; }
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof CompVisitor ) return ((CompVisitor<? extends T>)visitor).visitBlock(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BlockContext block() throws RecognitionException {
		BlockContext _localctx = new BlockContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_block);
		int _la;
		try {
			setState(141);
			switch (_input.LA(1)) {
			case SCAN:
			case PRINT:
			case IF:
			case WHILE:
			case ID:
				enterOuterAlt(_localctx, 1);
				{
				setState(126); stmt();
				setState(128);
				switch ( getInterpreter().adaptivePredict(_input,15,_ctx) ) {
				case 1:
					{
					setState(127); match(SEMICOLON);
					}
					break;
				}
				}
				break;
			case RBR:
				enterOuterAlt(_localctx, 2);
				{
				setState(130); match(RBR);
				setState(137);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << SCAN) | (1L << PRINT) | (1L << IF) | (1L << WHILE) | (1L << ID))) != 0)) {
					{
					{
					setState(131); stmt();
					setState(133);
					_la = _input.LA(1);
					if (_la==SEMICOLON) {
						{
						setState(132); match(SEMICOLON);
						}
					}

					}
					}
					setState(139);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(140); match(LBR);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public boolean sempred(RuleContext _localctx, int ruleIndex, int predIndex) {
		switch (ruleIndex) {
		case 2: return expr_sempred((ExprContext)_localctx, predIndex);
		}
		return true;
	}
	private boolean expr_sempred(ExprContext _localctx, int predIndex) {
		switch (predIndex) {
		case 0: return 14 >= _localctx._p;

		case 1: return 13 >= _localctx._p;

		case 2: return 12 >= _localctx._p;

		case 3: return 11 >= _localctx._p;

		case 4: return 10 >= _localctx._p;

		case 5: return 9 >= _localctx._p;
		}
		return true;
	}

	public static final String _serializedATN =
		"\2\3+\u0092\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\3\2\3\2\5\2\r\n\2\6\2\17\n"+
		"\2\r\2\16\2\20\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\3\5\3*\n\3\3\3\3\3\3\3\3\3\3\3\3\3"+
		"\7\3\62\n\3\f\3\16\3\65\13\3\3\3\3\3\3\3\3\3\5\3;\n\3\3\3\3\3\5\3?\n\3"+
		"\3\3\3\3\3\3\3\3\5\3E\n\3\3\3\3\3\5\3I\n\3\3\3\3\3\5\3M\n\3\3\3\3\3\5"+
		"\3Q\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4a\n"+
		"\4\3\4\3\4\5\4e\n\4\3\4\3\4\5\4i\n\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3"+
		"\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\7\4|\n\4\f\4\16\4\177\13\4\3\5\3\5"+
		"\5\5\u0083\n\5\3\5\3\5\3\5\5\5\u0088\n\5\7\5\u008a\n\5\f\5\16\5\u008d"+
		"\13\5\3\5\5\5\u0090\n\5\3\5\2\6\2\4\6\b\2\6\3%&\3\t\n\3\7\b\4\6\6\13\17"+
		"\u00b0\2\16\3\2\2\2\4P\3\2\2\2\6h\3\2\2\2\b\u008f\3\2\2\2\n\f\5\4\3\2"+
		"\13\r\7\21\2\2\f\13\3\2\2\2\f\r\3\2\2\2\r\17\3\2\2\2\16\n\3\2\2\2\17\20"+
		"\3\2\2\2\20\16\3\2\2\2\20\21\3\2\2\2\21\3\3\2\2\2\22\23\7\34\2\2\23Q\5"+
		"\6\4\2\24\25\7\33\2\2\25Q\7(\2\2\26\27\7\33\2\2\27\30\7(\2\2\30\31\7\30"+
		"\2\2\31\32\5\6\4\2\32\33\7\31\2\2\33Q\3\2\2\2\34\35\7(\2\2\35\36\7\22"+
		"\2\2\36Q\5\6\4\2\37 \7(\2\2 !\7\30\2\2!\"\5\6\4\2\"#\7\31\2\2#$\7\22\2"+
		"\2$%\5\6\4\2%Q\3\2\2\2&\'\7(\2\2\')\7\30\2\2(*\5\6\4\2)(\3\2\2\2)*\3\2"+
		"\2\2*+\3\2\2\2+,\7\31\2\2,-\7\22\2\2-.\7\26\2\2.\63\5\6\4\2/\60\7\32\2"+
		"\2\60\62\5\6\4\2\61/\3\2\2\2\62\65\3\2\2\2\63\61\3\2\2\2\63\64\3\2\2\2"+
		"\64\66\3\2\2\2\65\63\3\2\2\2\66\67\7\27\2\2\67Q\3\2\2\28:\7\35\2\29;\7"+
		"\24\2\2:9\3\2\2\2:;\3\2\2\2;<\3\2\2\2<>\5\6\4\2=?\7\25\2\2>=\3\2\2\2>"+
		"?\3\2\2\2?@\3\2\2\2@A\7\36\2\2AD\5\b\5\2BC\7\37\2\2CE\5\b\5\2DB\3\2\2"+
		"\2DE\3\2\2\2EQ\3\2\2\2FH\7 \2\2GI\7\24\2\2HG\3\2\2\2HI\3\2\2\2IJ\3\2\2"+
		"\2JL\5\6\4\2KM\7\25\2\2LK\3\2\2\2LM\3\2\2\2MN\3\2\2\2NO\5\b\5\2OQ\3\2"+
		"\2\2P\22\3\2\2\2P\24\3\2\2\2P\26\3\2\2\2P\34\3\2\2\2P\37\3\2\2\2P&\3\2"+
		"\2\2P8\3\2\2\2PF\3\2\2\2Q\5\3\2\2\2RS\b\4\1\2ST\7(\2\2TU\7\30\2\2UV\5"+
		"\6\4\2VW\7\31\2\2Wi\3\2\2\2Xi\7(\2\2YZ\7\24\2\2Z[\5\6\4\2[\\\7\25\2\2"+
		"\\i\3\2\2\2]i\7#\2\2^i\t\2\2\2_a\7\b\2\2`_\3\2\2\2`a\3\2\2\2ab\3\2\2\2"+
		"bi\7\3\2\2ce\7\b\2\2dc\3\2\2\2de\3\2\2\2ef\3\2\2\2fi\7\4\2\2gi\7\5\2\2"+
		"hR\3\2\2\2hX\3\2\2\2hY\3\2\2\2h]\3\2\2\2h^\3\2\2\2h`\3\2\2\2hd\3\2\2\2"+
		"hg\3\2\2\2i}\3\2\2\2jk\6\4\2\3kl\t\3\2\2l|\5\6\4\2mn\6\4\3\3no\t\4\2\2"+
		"o|\5\6\4\2pq\6\4\4\3qr\t\5\2\2r|\5\6\4\2st\6\4\5\3tu\7!\2\2u|\5\6\4\2"+
		"vw\6\4\6\3wx\7\"\2\2x|\5\6\4\2yz\6\4\7\3z|\5\6\4\2{j\3\2\2\2{m\3\2\2\2"+
		"{p\3\2\2\2{s\3\2\2\2{v\3\2\2\2{y\3\2\2\2|\177\3\2\2\2}{\3\2\2\2}~\3\2"+
		"\2\2~\7\3\2\2\2\177}\3\2\2\2\u0080\u0082\5\4\3\2\u0081\u0083\7\21\2\2"+
		"\u0082\u0081\3\2\2\2\u0082\u0083\3\2\2\2\u0083\u0090\3\2\2\2\u0084\u008b"+
		"\7\26\2\2\u0085\u0087\5\4\3\2\u0086\u0088\7\21\2\2\u0087\u0086\3\2\2\2"+
		"\u0087\u0088\3\2\2\2\u0088\u008a\3\2\2\2\u0089\u0085\3\2\2\2\u008a\u008d"+
		"\3\2\2\2\u008b\u0089\3\2\2\2\u008b\u008c\3\2\2\2\u008c\u008e\3\2\2\2\u008d"+
		"\u008b\3\2\2\2\u008e\u0090\7\27\2\2\u008f\u0080\3\2\2\2\u008f\u0084\3"+
		"\2\2\2\u0090\t\3\2\2\2\25\f\20)\63:>DHLP`dh{}\u0082\u0087\u008b\u008f";
	public static final ATN _ATN =
		ATNSimulator.deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
	}
}