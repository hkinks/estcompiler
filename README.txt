===Starting to develop===
For resolving dependencies on other libraries, it is using ivy. There has been added an
ant build file, which will install ivy and download the latest libraries.

===Description===
Simple programming language with estonian syntax for IAG0450 Analysis of Programming Languages with the intention
in mind to be flexible and forgiving in syntax so that it could be used to teach basic programming principles for example.

===About syntax===
*semicolon to end line is optional
*there are no variable types. However it creates some problems, ie. false == "false", "1" == 1.
*concatenation without any separating symbol. ie 'kirjuta "a väärtus on " a;'

===Usage===
To interpret source code:
java -jar estcompiler.jar <source>

To draw parse tree from source code:
java -jar estcompiler.jar <source> --tree
